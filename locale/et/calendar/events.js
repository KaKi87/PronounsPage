const {Event, day, week, month, dayYear, EventLevel} = require("../../../src/calendar/helpers");

module.exports = [
    new Event('Abieluvõrdsuse päev (Eesti)', '_hrc', 6, day(20), EventLevel.Day, [], null, null, y => y >= 2023),
];
