import fetch from 'node-fetch';

export const validateCaptcha = async (token) => {
    const res = await fetch('https://challenges.cloudflare.com/turnstile/v0/siteverify', {
        method: 'POST',
        headers: {
            'content-type': 'application/x-www-form-urlencoded',
        },
        body: `response=${encodeURIComponent(token)}&secret=${encodeURIComponent(process.env.TURNSTILE_SECRET)}`
    });
    const body = await res.json();
    return body['success'];
}
